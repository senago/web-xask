document.onreadystatechange = (() => {
  if (document.readyState === 'complete') {
    if (messages && messages.childElementCount != 0) {
      showMessages();
    }
    best_members();
    popular_tags();
  }
});