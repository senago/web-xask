const messages = document.querySelector("ul.messages");
const pxToVHScalar = 100 / document.documentElement.clientHeight;

function createMessage(msg, level) {
  let message = document.createElement("li");
  message.className = level
  message.innerText = msg;
  messages.append(message);
}

function showMessage(msg, level) {
  let message = document.createElement("li");
  message.className = level
  message.innerText = msg;
  messages.append(message);
  showMessages();
}

var timeoutClearMessagesID;
function showMessages() {
  clearTimeout(timeoutClearMessagesID);
  showPromise().then(timeoutClearMessagesID = setTimeout(() => { clearMessages() }, 3000));
}

function showPromise() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      messages.style.opacity = 1;
      messages.style.top = (99 - messages.clientHeight * pxToVHScalar) + "vh";
      resolve();
    }, 10);
  });
}

function clearMessages() {
  messages.style.opacity = 0;
  messages.style.top = "110vh";
  setTimeout(() => {
    while (messages.firstChild) {
      messages.removeChild(messages.firstChild);
    }
  }, 500);
}
