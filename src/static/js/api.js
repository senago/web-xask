const bestMembers = document.getElementById('best-members');
const popularTags = document.getElementById('popular-tags');

function fetchAPI(url, method, body) {
  csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
  return fetch(url, {
    method: method,
    body: body,
    credentials: "include",
    headers: {
      "X-CSRFToken": csrftoken,
      "Accept": "application/json",
      "Content-Type": "application/json",
    }
  });
}

async function vote(pk, vote_status) {
  let body = JSON.stringify({ post_id: pk, vote_status: vote_status });
  let response = await fetchAPI("/api/vote/", "POST", body);
  if (!response.ok) {
    showMessage(`Failed`, 'error');
    return;
  }

  let json = await response.json();
  if (!response.ok) {
    showMessage(`Failed to vote: ${json}`, 'error');
  } else {
    location.reload();
  }
}

async function accept(question_id, answer_id) {
  let body = JSON.stringify({ question_id: question_id, answer_id: answer_id });
  let response = await fetchAPI("/api/accept/", "POST", body);
  if (!response.ok) {
    showMessage(`Failed`, 'error');
    return;
  }

  if (!response.ok) {
    showMessage(`Failed to accept`, 'error');
  } else {
    location.reload();
  }
}

$("#select-by-tags").keyup(function (event) {
  if (event.keyCode == 13) {
    let tags = event.target.value.split(" ");
    if (!tags) {
      showMessage('Empty tags', 'error');
    } else {
      window.location.href = `/?tags=${tags}`;
    }
  }
});

async function best_members() {
  let response = await fetchAPI("/api/best_members/", "POST");

  if (!response.ok) {
    showMessage(`Failed to accept`, 'error');
  } else {
    let json = await response.json();
    set_best_members(json['best_members']);
  }
}

async function set_best_members(best_members) {
  best_members.forEach(member => {
    let child = document.createElement("a");
    child.className = "member";
    child.innerText = member['username'];
    child.href = `user/${member['username']}`;
    bestMembers.appendChild(child);
  });
  bestMembers.style.opacity = 1;
}

async function popular_tags() {
  let response = await fetchAPI("/api/popular_tags/", "POST");

  if (!response.ok) {
    showMessage(`Failed to accept`, 'error');
  } else {
    let json = await response.json();
    set_popular_tags(json['popular_tags']);
  }
}

async function set_popular_tags(popular_tags) {
  popular_tags.forEach(tag => {
    let child = document.createElement("div");
    child.className = "tag";
    child.innerText = `${tag['name']}-${tag['count']}`;
    child.onclick = () => { window.location.replace(`?tags=${tag['name']}`); };
    popularTags.appendChild(child);
  });
  popularTags.style.opacity = 1;
}
