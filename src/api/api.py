from question.models import Answer, Question, QuestionsRegistry, Vote
from rest_framework import status
from api.utils import ConvertVoteStatus
from question.models import PostAbstract
from rest_framework.response import Response
from rest_framework.decorators import api_view

from user.models import UsersRegistry


@api_view(['POST'])
def api_vote(request):
    if not request.user.is_authenticated:
        return Response(data="You have to login to vote", status=status.HTTP_401_UNAUTHORIZED)

    post_id = request.data['post_id']
    vote_status, err = ConvertVoteStatus(request.data['vote_status'])
    if err != None:
        return Response(data=str(err), status=status.HTTP_400_BAD_REQUEST)

    post = PostAbstract.objects.get(pk=post_id)
    if post.author == request.user:
        return Response(data="You cannot vote for your posts", status=status.HTTP_403_FORBIDDEN)

    vote, _ = Vote.objects.get_or_create(user=post.author, post=post)
    delta = vote.update_status(vote_status)

    return Response(data={'delta': delta}, status=status.HTTP_200_OK)


@api_view(['POST'])
def api_accept(request):
    if not request.user.is_authenticated:
        return Response(data="You have to login to accept an answer", status=status.HTTP_401_UNAUTHORIZED)

    answer_id = request.data['answer_id']
    question_id = request.data['question_id']

    question = PostAbstract.objects.get(pk=question_id)
    if question.author != request.user:
        return Response(data="You can accept answers only for your questions", status=status.HTTP_403_FORBIDDEN)

    answer = Answer.objects.get(pk=answer_id)
    if answer.related_question != question:
        return Response(data="Answer is for another question", status=status.HTTP_400_BAD_REQUEST)

    question.accepted_answer_id = answer_id
    question.status = Question.QUESTION_STATUS_ANSWERED
    question.save()

    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def api_best_members(request):
    return Response(data={'best_members': UsersRegistry().best_members}, status=status.HTTP_200_OK)


@api_view(['POST'])
def api_popular_tags(request):
    return Response(data={'popular_tags': QuestionsRegistry().popular_tags}, status=status.HTTP_200_OK)
