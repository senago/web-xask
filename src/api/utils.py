from question.models import Vote


def ConvertVoteStatus(str: str):
    for k, v in Vote.VOTE_STATUS:
        if v == str:
            return k, None
    return None, ValueError(f"failed to convert [{str}] to vote status")
