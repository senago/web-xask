from django.urls import path
from api.api import api_accept, api_vote, api_best_members, api_popular_tags

app_name = 'api'

urlpatterns = [
    path('vote/', api_vote, name='vote'),
    path('accept/', api_accept, name='accept'),
    path('best_members/', api_best_members, name='best_members'),
    path('popular_tags/', api_popular_tags, name='popular_tags')
]
