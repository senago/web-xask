import factory
import factory.fuzzy
from factory.django import DjangoModelFactory
from user.models import User
from question.models import Answer, PostAbstract, Question, Tag, Vote

VOTE_STATUSES = [p[0] for p in Vote.VOTE_STATUS]
QUESTION_STATUSES = [p[0] for p in Question.QUESTION_STATUS]


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Sequence(lambda n: "mail%d@example.com" % n)
    username = factory.Sequence(lambda n: "user%d" % n)


class TagFactory(DjangoModelFactory):
    class Meta:
        model = Tag

    name = factory.Sequence(lambda n: "tag%d" % n)


class PostAbstractFactory(DjangoModelFactory):
    class Meta:
        model = PostAbstract

    body = factory.Faker('paragraph')
    author = factory.SubFactory(UserFactory)


class QuestionFactory(PostAbstractFactory):
    class Meta:
        model = Question

    title = factory.Sequence(lambda n: "title%d" % n)
    status = factory.fuzzy.FuzzyChoice(QUESTION_STATUSES)


class AnswerFactory(PostAbstractFactory):
    class Meta:
        model = Answer

    related_question = factory.SubFactory(QuestionFactory)


class VoteFactory(DjangoModelFactory):
    class Meta:
        model = Vote

    user = factory.SubFactory(UserFactory)
    post = factory.SubFactory(PostAbstractFactory)
    status = factory.fuzzy.FuzzyChoice(VOTE_STATUSES)
