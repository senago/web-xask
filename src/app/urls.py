from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings
from question.views import HomeListView

from user.views import (
    RegisterView,
)

urlpatterns = [
    path('', HomeListView.as_view(), name='home'),

    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('register/', RegisterView.as_view(), name='register'),
    path('user/', include('user.urls', 'user')),

    path('admin/', admin.site.urls),
    path('api/', include('api.urls', 'api')),
    path('question/', include('question.urls', 'question')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
