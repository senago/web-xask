import logging
from os import confstr
from tqdm import tqdm
import multiprocessing as mp
from django.db import transaction
from django.core.management import call_command
from django.core.management.base import BaseCommand
from app.factories import AnswerFactory, QuestionFactory, TagFactory, UserFactory, VoteFactory


# disable logging
logger = logging.getLogger('factory')
logger.propagate = False

logger = logging.getLogger('faker')
logger.propagate = False

votes = []
answers = []


def build_answers(size):
    return AnswerFactory.build_batch(size=size)


def collect_answers(result):
    answers.extend(result)


def build_votes(size):
    return VoteFactory.build_batch(size=size)


def collect_votes(result):
    votes.extend(result)


class Command(BaseCommand):
    help = 'Fill the database with some data'

    @transaction.atomic
    def handle(self, *args, **kwargs):
        NUM_USERS = 10**2
        NUM_TAGS = NUM_USERS
        NUM_QUESTIONS = NUM_USERS * 10
        NUM_ANSWERS = NUM_QUESTIONS * 10

        NUM_VOTES_PER_USER = 2 * 10**2
        NUM_VOTES = NUM_VOTES_PER_USER * NUM_USERS

        CPU_COUNT = mp.cpu_count()

        self.stdout.write('Cleaning up...')
        self.cleanup()

        # --------------------------- USERS --------------------------- #
        self.stdout.write(f'Creating {NUM_USERS} users...')
        users = UserFactory.create_batch(size=NUM_USERS)

        # --------------------------- TAGS --------------------------- #
        self.stdout.write(f'Creating {NUM_TAGS} tags...')
        tags = TagFactory.create_batch(size=NUM_TAGS)

        # --------------------------- QUESTIONS --------------------------- #
        self.stdout.write(f'Building {NUM_QUESTIONS} questions...')
        questions = QuestionFactory.build_batch(size=NUM_QUESTIONS)

        self.stdout.write('Setting questions author...')
        for i, question in tqdm(enumerate(questions), total=NUM_QUESTIONS):
            question.author = users[i % NUM_USERS]

        self.stdout.write('Saving questions...')
        for i, question in tqdm(enumerate(questions), total=NUM_QUESTIONS):
            question.save()

        self.stdout.write('Setting questions tags...')
        for i, question in tqdm(enumerate(questions), total=NUM_QUESTIONS):
            question.tags.set(tags[(i % NUM_TAGS):((i + 3) % NUM_TAGS)])

        self.stdout.write('Saving questions...')
        for i, question in tqdm(enumerate(questions), total=NUM_QUESTIONS):
            question.save()

        # --------------------------- ANSWERS --------------------------- #
        self.stdout.write(f'Building {NUM_ANSWERS} answers in parallel...')

        pool = mp.Pool()
        for i in range(CPU_COUNT):
            pool.apply_async(build_answers, args=(NUM_ANSWERS // CPU_COUNT, ), callback=collect_answers)
        pool.close()
        pool.join()

        NUM_ANSWERS = len(answers)
        self.stdout.write(f'Built {NUM_ANSWERS} votes')

        self.stdout.write('Setting answers...')
        for i, answer in tqdm(enumerate(answers), total=NUM_ANSWERS):
            answer.author = users[i % NUM_USERS]
            answer.related_question = questions[i % NUM_QUESTIONS]

        self.stdout.write('Saving answers...')
        for i, answer in tqdm(enumerate(answers), total=NUM_ANSWERS):
            answer.save()

        # --------------------------- VOTES --------------------------- #
        self.stdout.write(f'Building {NUM_VOTES} votes in parallel...')

        pool = mp.Pool()
        for i in range(CPU_COUNT):
            pool.apply_async(build_votes, args=(NUM_VOTES // CPU_COUNT, ), callback=collect_votes)
        pool.close()
        pool.join()

        NUM_VOTES = len(votes)
        self.stdout.write(f'Built {NUM_VOTES} votes')

        posts = questions + answers
        amount_of_posts = NUM_QUESTIONS + NUM_ANSWERS

        self.stdout.write('Setting votes...')
        for i in tqdm(range(NUM_USERS)):
            offset = i * NUM_VOTES_PER_USER
            for j in range(NUM_VOTES_PER_USER):
                user = users[i % NUM_USERS]
                post = posts[(offset + NUM_USERS + j) % amount_of_posts]
                if post.author != user:
                    vote = votes[offset + j]

                    vote.user = user
                    vote.post = post

                    user.score += vote.status
                    post.score += vote.status

                    vote.save()

        self.stdout.write('Saving questions after applied votes...')
        for i, question in tqdm(enumerate(questions), total=NUM_QUESTIONS):
            question.save(update_fields=['score'])

        self.stdout.write('Saving answers after applied votes...')
        for i, answer in tqdm(enumerate(answers), total=NUM_ANSWERS):
            answer.save(update_fields=['score'])

        self.stdout.write('Saving users after applied votes...')
        for i, user in tqdm(enumerate(users), total=NUM_USERS):
            user.save(update_fields=['score'])

        self.stdout.write('Restoring superuser...')
        call_command('createsuperuser', '--noinput')

    def cleanup(self):
        call_command('flush', '--no-input')
