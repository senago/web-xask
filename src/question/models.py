from django.db import models
from django.urls import reverse
from django.conf import settings
from django.db import transaction
from django.utils.text import slugify
from django_lifecycle.hooks import AFTER_SAVE
from polymorphic.models import PolymorphicModel
from polymorphic.managers import PolymorphicManager
from django_lifecycle import LifecycleModelMixin, hook, BEFORE_SAVE, BEFORE_DELETE, AFTER_CREATE
from app.common import SingletonMeta
from django.db.models import Count


class QuestionsRegistry(metaclass=SingletonMeta):
    POPULAR_TAGS_AMOUNT = 5

    def __init__(self):
        self.popular_tags = Tag.objects.all().annotate(count=Count('questions')).order_by(
            '-count')[:self.POPULAR_TAGS_AMOUNT].values('name', 'count')

    def update_popular_tags(self, name, count):
        for tag in reversed(self.popular_tags):
            if tag['count'] < count:
                tag['count'] = count
                tag['name'] = name
                break


class Tag(models.Model):
    name = models.CharField(max_length=32, null=False, blank=False, unique=True)

    def __str__(self):
        return self.name


class PostAbstractManager(PolymorphicManager):
    def get_queryset(self):
        return super(PostAbstractManager, self).get_queryset()

    def get_ordered_queryset(self, request):
        ordering = 'desc'
        if 'ordering' in request.GET:
            ordering = request.GET['ordering']

        order_by = 'created_at'
        if 'order_by' in request.GET:
            order_by = request.GET['order_by']

        if 'tags' in request.GET:
            qs = self.get_queryset().filter(tags__name__in=request.GET['tags'].split(','))
        else:
            qs = self.get_queryset()

        if ordering == 'asc':
            qs = qs.order_by(order_by)
        else:
            qs = qs.order_by('-' + order_by)

        return qs


class PostAbstract(PolymorphicModel):
    objects = PostAbstractManager()

    body = models.TextField(max_length=2048, null=False, blank=False)
    score = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class Question(LifecycleModelMixin, PostAbstract):
    QUESTION_STATUS_OPEN = 0
    QUESTION_STATUS_CLOSED = 1
    QUESTION_STATUS_ANSWERED = 2

    QUESTION_STATUS = (
        (QUESTION_STATUS_OPEN, 'open'),
        (QUESTION_STATUS_CLOSED, 'closed'),
        (QUESTION_STATUS_ANSWERED, 'answered')
    )

    title = models.CharField(max_length=128, null=False, blank=False)
    slug = models.SlugField(blank=True, unique=True)

    status = models.IntegerField(choices=QUESTION_STATUS, default=QUESTION_STATUS_OPEN)
    accepted_answer_id = models.IntegerField(blank=True, null=True)

    tags = models.ManyToManyField(Tag, blank=True, default=None, related_name='questions')

    def __str__(self):
        return '@'.join([self.title, self.author.username])

    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})

    def update_tags(self, tag_names, commit=False):
        for tag_name in tag_names:
            tag, _ = Tag.objects.get_or_create(name=tag_name)
            self.tags.add(tag)
        if commit:
            self.save()

    @hook(BEFORE_SAVE)
    def before_save(self):
        if not self.slug:
            self.slug = slugify(self.author.username + "-" + self.title)

    # TODO:
    # @hook(AFTER_SAVE)
    # def before_save(self):
    #     for tag in self.tags.all():
    #         QuestionsRegistry().update_popular_tags(tag.name,   tag.annotate(count=Count('questions'))['count'])

    def type(self):
        return self.__class__.__name__


class Answer(PostAbstract):
    # choosing such naming to avoid issues with circular links
    related_question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')

    def type(self):
        return self.__class__.__name__


class Vote(LifecycleModelMixin, models.Model):
    VOTE_STATUS_NONE = 0
    VOTE_STATUS_UPVOTE = 1
    VOTE_STATUS_DOWNVOTE = -1

    VOTE_STATUS = (
        (VOTE_STATUS_NONE, 'none'),
        (VOTE_STATUS_UPVOTE, 'upvote'),
        (VOTE_STATUS_DOWNVOTE, 'downvote')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='votes')
    post = models.ForeignKey(PostAbstract, on_delete=models.CASCADE, related_name='votes')
    status = models.IntegerField(choices=VOTE_STATUS, default=VOTE_STATUS_NONE)

    def __str__(self):
        return '{}-{}-{}'.format(self.user.username, self.status, self.post.pk)

    @transaction.atomic
    def update_related_scores(self, delta: int):
        self.user.score += delta
        self.post.score += delta
        self.user.save()
        self.post.save()

    def update_status(self, status: int) -> int:
        if self.status == status:
            self.delete()
            return -self.status

        delta = status - self.status
        self.update_related_scores(delta=delta)

        self.status = status
        self.save()

        return delta

    @hook(BEFORE_DELETE)
    def before_delete(self):
        if not self.status == self.VOTE_STATUS_NONE:
            self.update_related_scores(-self.status)
