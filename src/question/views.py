from django.db.models import constraints
from django.http.response import HttpResponseForbidden, HttpResponseRedirect
from django.urls.base import reverse
from django.views.generic import CreateView, ListView, FormView
from django.views.generic.detail import DetailView, SingleObjectMixin
from question.forms import CreateQuestionForm, AnswerForm
from question.models import Question
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages


class HomeListView(ListView):
    model = Question
    paginate_by = 5
    template_name = 'question/home.html'

    def get_queryset(self):
        return self.model.objects.get_ordered_queryset(self.request)


class CreateQuestionView(CreateView):
    model = Question
    form_class = CreateQuestionForm
    template_name = 'question/question_form.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, 'You have to login first')
            return HttpResponseRedirect(reverse('login'))
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, 'Successfully posted the question')
        return reverse('home')

    def form_valid(self, form):
        question = form.save(commit=False)

        question.author = self.request.user
        question.save()

        tag_names = form.cleaned_data['tags'].split()
        question.update_tags(tag_names)

        form.save_m2m()
        return super().form_valid(form)


class QuestionDetailView(DetailView):
    model = Question
    template_name = 'question/question_detail.html'

    def get_object(self):
        return super().get_object()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = AnswerForm()
        return context


class QuestionAnswerFormView(SingleObjectMixin, FormView):
    model = Question
    form_class = AnswerForm
    template_name = 'question/question_detail.html'

    def get_success_url(self):
        return self.request.path

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        answer = form.save(commit=False)

        answer.author = self.request.user
        answer.related_question = self.object

        answer.save()
        form.save_m2m()

        return super().form_valid(form)


class QuestionView(View):
    def get(self, request, *args, **kwargs):
        view = QuestionDetailView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = QuestionAnswerFormView.as_view()
        return view(request, *args, **kwargs)
