from django.contrib import admin

from question.models import Tag, Answer, Question, Vote

admin.site.register(Tag)
admin.site.register(Answer)
admin.site.register(Question)
admin.site.register(Vote)
