from django.urls import path
from question.views import (
    CreateQuestionView,
    QuestionView,
)

app_name = 'question'

urlpatterns = [
    path('create/', CreateQuestionView.as_view(), name='create'),
    path('<slug:slug>/', QuestionView.as_view(), name='detail'),
]
