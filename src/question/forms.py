from django import forms
from django.forms.models import ModelForm
from question.models import Answer, Question


class CreateQuestionForm(ModelForm):
    tags = forms.CharField(max_length=64, strip=True, required=False)

    class Meta:
        model = Question
        fields = ['title', 'body']


class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ['body']
