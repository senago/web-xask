from logging import log
from django.urls.base import reverse
from user.models import User
from django.urls import reverse_lazy
from user.forms import UserRegisterForm
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.http.response import HttpResponseForbidden, HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http.response import HttpResponseForbidden
from django.contrib import messages


class RegisterView(SuccessMessageMixin, CreateView):
    form_class = UserRegisterForm

    template_name = 'user/register.html'
    success_url = reverse_lazy('login')


class UserDetailView(DetailView):
    def get_object(self):
        return get_object_or_404(User, slug=self.kwargs['slug'])

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class UserUpdateView(UpdateView):
    model = User
    fields = ['photo']

    template_name = 'user/settings.html'
    success_url = reverse_lazy('home')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        user = self.get_object()
        if user != request.user:
            messages.error(request, 'Forbidden')
            return HttpResponseRedirect(reverse('home'))
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        request.user.photo.delete(save=True)
        return super().post(request, *args, **kwargs)
