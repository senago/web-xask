from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django_lifecycle import LifecycleModelMixin, hook, AFTER_DELETE, BEFORE_SAVE
from django_lifecycle.hooks import BEFORE_UPDATE
from app.common import SingletonMeta


class UsersRegistry(metaclass=SingletonMeta):
    BEST_MEMBERS_AMOUNT = 5

    def __init__(self):
        self.best_members = list(User.objects.order_by(
            'score')[:self.BEST_MEMBERS_AMOUNT].values('username', 'score'))

    def update_best_members(self, username, score):
        for member in reversed(self.best_members):
            if member['score'] < score:
                member['score'] = score
                member['username'] = username
                break


class UserManager(BaseUserManager):
    def create_user(self, email, username, password, photo=None):
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        if photo:
            pass  # TODO

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=email,
            username=username,
            password=password,
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


def upload_location(instance, filename):
    filepath = 'uploads/{}/{}'.format(str(instance.slug), filename)
    return filepath


class User(LifecycleModelMixin, AbstractBaseUser):
    email = models.EmailField(max_length=64, unique=True)
    username = models.CharField(max_length=32, unique=True)
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    score = models.IntegerField(default=0)
    slug = models.SlugField(blank=True, unique=True)
    photo = models.ImageField(upload_to=upload_location, null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    @hook(BEFORE_SAVE)
    def before_save(self):
        if not self.slug:
            self.slug = slugify(self.username)
        UsersRegistry().update_best_members(self.username, self.score)

    @hook(AFTER_DELETE)
    def after_delete(self):
        self.image.delete(False)
