from django.urls import path
from user.views import (
    UserDetailView,
    UserUpdateView,
)

app_name = 'user'

urlpatterns = [
    path('<slug>/', UserDetailView.as_view(), name='detail'),
    path('<slug>/settings/', UserUpdateView.as_view(), name='settings'),
]
